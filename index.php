<?php
// Create database connection using config file
include_once("config.php");
 
// Fetch all users data from database
$result = mysqli_query($mysqli, "SELECT * FROM coffee ORDER BY id DESC");
?>
 
<html>
<head>
    
<link rel="stylesheet" href="style.css">   
    <title>Homepage</title>
</head>
 
<body>
<a href="add.php">Add New User</a><br/><br/>
 
    <table width='80%' border=1>
 
    <tr>
        <th rowspan="2">No</th>
        <th colspan="2">Menu</th>
        <th rowspan="2">Harga</th>
        <th rowspan="2">Update</th>
    </tr>
    <tr>
        <th>Coffee</th>
        <th>Daerah</th>
    </tr>

    <?php 
    $i=1; 
    while($user_data = mysqli_fetch_array($result)) {         
        echo "<tr>";
        echo "<td>".$i++."</td>";
        echo "<td>".$user_data['nama']."</td>";
        echo "<td>".$user_data['daerah']."</td>";
        echo "<td>".$user_data['harga']."</td>";    
        echo "<td><a href='edit.php?id=$user_data[id]'>Edit</a> | <a href='delete.php?id=$user_data[id]'>Delete</a></td></tr>";        
    }
    ?>
    </table>
</body>
</html>